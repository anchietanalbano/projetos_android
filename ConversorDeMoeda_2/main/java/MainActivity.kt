package com.anchietaalbano.a04_conversordemoeda_2

import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.anchietaalbano.a04_conversordemoeda_2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonDolar.setOnClickListener{
            escrever(0.8)
        }
        binding.buttonReal.setOnClickListener{
            escrever(5.0)
        }
        binding.buttonPeso.setOnClickListener{
            escrever(10.2)
        }
    }

    private fun escrever(taxa: Double) {
        val euro = binding.editEuros.text.toString().trim()

        if(!euro.isEmpty()){
            val result = euro.toDouble()*taxa
            Toast.makeText(applicationContext,"$$result",Toast.LENGTH_SHORT).show()

        }
    }
}