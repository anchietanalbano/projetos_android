package com.anchietaalbano.aplication_test_firebase.view.formlogin

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Contacts

import androidx.appcompat.app.AppCompatActivity

import com.anchietaalbano.aplication_test_firebase.databinding.ActivityFormLoginBinding

import com.anchietaalbano.aplication_test_firebase.view.formcadastro.Formcadastro
import com.anchietaalbano.aplication_test_firebase.view.telaPrincipal.TelaPrincipal
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException

class FormLoginActivity : AppCompatActivity() {

    private val auth = FirebaseAuth.getInstance()
    private lateinit var binding: ActivityFormLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomEnter.setOnClickListener {
            val email = binding.editEmail.text.toString()
            val senha = binding.editSenha.text.toString()

            if(email.isEmpty() || senha.isEmpty()){
                Snackbar.make(it, "Preencha todos os campos!", Snackbar.LENGTH_LONG).setBackgroundTint(
                    Color.RED).show()
            }else{
                auth.signInWithEmailAndPassword(email, senha).addOnCompleteListener {autentica ->
                    if(autentica.isSuccessful){
                        navegarTelaPrincipal()
                    }
                }.addOnFailureListener{view ->

                    val messag = when(view){

                        is FirebaseAuthWeakPasswordException -> "Senha inválida!"
                        is FirebaseAuthInvalidCredentialsException -> "Email inválido!"
                        is FirebaseNetworkException -> "Sem conexão com a internet!"
                        else -> "Erro ao conectar o usuário!" //erro generico
                    }

                    Snackbar.make(it, messag, Snackbar.LENGTH_LONG).setBackgroundTint(
                        Color.RED).show()
                }
            }
        }
        binding.textTelaCadastro.setOnClickListener{
            val intent = Intent(this, Formcadastro::class.java)
            startActivity(intent)
        }
    }

    private fun navegarTelaPrincipal(){
        val intent = Intent(this, TelaPrincipal::class.java)
        startActivity(intent)
        finish()
    }

    //ciclo de vida da aplicação
    override fun onStart() {
        super.onStart()
        val usuarioAtual = FirebaseAuth.getInstance().currentUser //currentUser atualmente autenticado
        // se tiver usuario logado, ele mantém o mesmo na tela principal
        if(usuarioAtual != null){
            navegarTelaPrincipal()
        }
    }
}

