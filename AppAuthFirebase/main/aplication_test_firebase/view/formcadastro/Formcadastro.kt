package com.anchietaalbano.aplication_test_firebase.view.formcadastro

import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.anchietaalbano.aplication_test_firebase.R
import com.anchietaalbano.aplication_test_firebase.databinding.ActivityFormcadastroBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Firebase
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthActionCodeException
import com.google.firebase.auth.FirebaseAuthEmailException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException

class Formcadastro : AppCompatActivity() {

    private lateinit var binding: ActivityFormcadastroBinding
    private val auth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormcadastroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomRegister.setOnClickListener{

            val email = binding.editEmail.text.toString()
            val senha = binding.editSenha.text.toString()

            if(email.isEmpty() || senha.isEmpty()){
                Snackbar.make(it,"Preencha todos os campos!", Snackbar.LENGTH_LONG).setBackgroundTint(
                    Color.RED).show()
            }else{
                auth.createUserWithEmailAndPassword(email, senha).addOnCompleteListener{ register ->
                    if(register.isSuccessful){
                        Snackbar.make(it,"Usuario cadastrado", Snackbar.LENGTH_LONG).setBackgroundTint(
                            Color.BLUE).show()
                        binding.editEmail.setText("")
                        binding.editSenha.setText("")
                    }
                }.addOnFailureListener{erro ->

                    val messag = when(erro){
                        // limite minimo o quantidade de caracteres da senha
                        is FirebaseAuthWeakPasswordException -> "É preciso 6 caracteres mínimos para uma senha válida!"
                        is FirebaseAuthInvalidCredentialsException -> "Digite um email válido!"
                        is FirebaseAuthUserCollisionException -> "A conta já foi cadastrada!"
                        is FirebaseNetworkException -> "Sem conexão com a internet!"
                        else -> "Erro ao cadastrar usuário!" //erro generico
                    }
                    Snackbar.make(it, messag , Snackbar.LENGTH_LONG).setBackgroundTint(Color.RED).show()
                }
            }
        }

    }
}