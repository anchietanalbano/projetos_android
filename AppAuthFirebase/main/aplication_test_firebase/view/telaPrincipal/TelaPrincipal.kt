package com.anchietaalbano.aplication_test_firebase.view.telaPrincipal

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.anchietaalbano.aplication_test_firebase.R
import com.anchietaalbano.aplication_test_firebase.databinding.ActivityTelaPrincipalBinding
import com.anchietaalbano.aplication_test_firebase.view.formlogin.FormLoginActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.integrity.internal.k
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth
import com.google.firebase.firestore.FirebaseFirestore


class TelaPrincipal : AppCompatActivity() {

    // A base de estudo foi construida com dados estáticos
    private lateinit var binding: ActivityTelaPrincipalBinding
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTelaPrincipalBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // saindo do intent tela principal
        binding.bottomExit.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val voltarTelaLogin = Intent(this, FormLoginActivity::class.java)
            startActivity(voltarTelaLogin)
            finish()
        }
        // gravando dados no banco de dados
        binding.btGravaDadosDB.setOnClickListener {

            val usuariosMap = hashMapOf(
                "nome" to "Maria",
                "sobrenome" to "Silva",
                "idade" to 49
            )

            //acessando o db firestore, criando coleções e documentos
            db.collection("Usuários").document("Maria").set(usuariosMap).addOnCompleteListener {
                Log.d("db", "Sucesso ao salvar no banco de dados do usuário!")
            }.addOnFailureListener {
                println("Erro ao gravar dados")
            }
        }

        // lendo arquivos do DB do firestore - acessando a coleção de usuários e depois documentos
        binding.btLerDadosDB.setOnClickListener {
            db.collection("Usuários").document("Marcos")
                .addSnapshotListener { document, error ->
                    if (document != null) {
                        val idade = document.getLong("idade")
                        binding.txtResultadoNome.text = document.getString("nome")
                        binding.txtResultadoSobrenome.text = document.getString("sobrenome")
                        binding.txtResultadoIdade.text = idade.toString()
                    }
                }
        }

        // atualização de dados do usuario
        binding.btAtualiDadosDB.setOnClickListener {
            db.collection("Usuários").document("Marcos")
                .update("sobrenome","Nascimento", "idade", 34 ).addOnCompleteListener {
                    Toast.makeText(this,"Sucesso ao atualizar os dados do usuário!", Toast.LENGTH_LONG).show()
                }
        }

        binding.btDeletarDadosDB.setOnClickListener {
            db.collection("Usuários").document("Maria")
                .delete().addOnCompleteListener {
                    Toast.makeText(this,"Sucesso ao deletar o dados do usuário!", Toast.LENGTH_LONG).show()
                }
        }
    }
}