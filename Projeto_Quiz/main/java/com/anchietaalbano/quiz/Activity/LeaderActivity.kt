package com.anchietaalbano.quiz.Activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.anchietaalbano.quiz.Adapter.LeaderAdapter
import com.anchietaalbano.quiz.Domain.UserModel
import com.anchietaalbano.quiz.R
import com.anchietaalbano.quiz.databinding.ActivityLeaderBinding
import com.bumptech.glide.Glide

class LeaderActivity : AppCompatActivity() {
    lateinit var binding: ActivityLeaderBinding

    private val leaderAdapter by lazy { LeaderAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLeaderBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val window: Window = this@LeaderActivity.window
        window.statusBarColor = ContextCompat.getColor(this@LeaderActivity, R.color.green)

        binding.apply {
            scoreTop1Txt.text = loadData().get(0).score.toString()
            scoreTop2Txt.text = loadData().get(1).score.toString()
            scoreTop3Txt.text = loadData().get(2).score.toString()
            titleTop1Txt.text = loadData().get(0).name
            titleTop2Txt.text = loadData().get(1).name
            titleTop3Txt.text = loadData().get(2).name

            val drawableResourceId1: Int = binding.root.resources.getIdentifier(
                loadData().get(0).pic, "drawable", binding.root.context.packageName
            )
            Glide.with(root.context)
                .load(drawableResourceId1)
                .into(pic1)

            val drawableResourceId2: Int = binding.root.resources.getIdentifier(
                loadData().get(1).pic, "drawable", binding.root.context.packageName
            )
            Glide.with(root.context)
                .load(drawableResourceId2)
                .into(pic2)

            val drawableResourceId3: Int = binding.root.resources.getIdentifier(
                loadData().get(2).pic, "drawable", binding.root.context.packageName
            )
            Glide.with(root.context)
                .load(drawableResourceId3)
                .into(pic3)


            bottonMenu.setItemSelected(R.id.Board)
            bottonMenu.setOnItemSelectedListener {
                if (it == R.id.home) {
                    startActivity(Intent(this@LeaderActivity, MainActivity::class.java))
                }
            }

            var listNo: MutableList<UserModel> = loadData()
            listNo.removeAt(0)
            listNo.removeAt(1)
            listNo.removeAt(2)


            leaderAdapter.differ.submitList(listNo)

            leaderView.apply {
                layoutManager = LinearLayoutManager(this@LeaderActivity)
                adapter = leaderAdapter
            }
        }

    }


    // you can get below list from your API service, this is a example list

    private fun loadData(): MutableList<UserModel> {
        val users: MutableList<UserModel> = mutableListOf()
        users.add(UserModel(0, "Sophia", "person1", 4850))
        users.add(UserModel(1, "Daniel", "person2", 4560))
        users.add(UserModel(2, "James", "person3", 3873))
        users.add(UserModel(3, "John Will", "person4", 3250))
        users.add(UserModel(4, "Emily Ross", "person5", 3015))
        users.add(UserModel(5, "David Brow", "person6", 2970))
        users.add(UserModel(6, "Sarah Wall", "person7", 2870))
        users.add(UserModel(7, "Michel David", "person8", 2670))
        users.add(UserModel(8, "Sarah Wilson", "person9", 2380))
        users.add(UserModel(9, "Sarah Wilson", "person9", 2380))
        return users
    }
}