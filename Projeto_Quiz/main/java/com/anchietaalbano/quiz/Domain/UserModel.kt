package com.anchietaalbano.quiz.Domain

import kotlin.String

data class UserModel(
    val id:Int,
    val name: String,
    val pic: String,
    val score:Int
)
