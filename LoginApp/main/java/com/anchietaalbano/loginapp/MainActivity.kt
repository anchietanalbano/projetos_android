package com.anchietaalbano.loginapp

import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.anchietaalbano.loginapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonEntrar.setOnClickListener{
            val username = binding.editName.text.toString().trim()
            val password = binding.editPassword.text.toString().trim()

            if(username.equals("user") && password.equals("pass")){
                Toast.makeText(applicationContext, "Login OK", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(applicationContext, "login Inválido", Toast.LENGTH_SHORT).show()
            }

            binding.editName.setText("")
            binding.editPassword.setText("")
        }
    }
}