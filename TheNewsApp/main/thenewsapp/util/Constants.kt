package com.anchietaalbano.thenewsapp.util

class Constants {
    companion object{
        const val API_KEY = "063dfc05eae047c79b937d1fbac1fcce"
        const val BASE_URL = "https://newsapi.org/"
        const val SEARCH_TIME_DELAY = 500L
        const val QUERY_PAGE_SIZE = 20
    }

}