package com.anchietaalbano.thenewsapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.anchietaalbano.thenewsapp.models.Article

@Dao
interface ArticleDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE) //verificar se tem algum artigo antigo e subescreve
    suspend fun upsert(article: Article): Long // suspend executa a partir de uma rotina

    @Query("SELECT * FROM articles") //Mostra todos os artigos na tabela
    fun getAllArticles(): LiveData<List<Article>>

    @Delete
    suspend fun deleteArticle(article: Article) //Deleta no segundo plano 
}