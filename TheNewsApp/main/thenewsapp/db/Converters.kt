package com.anchietaalbano.thenewsapp.db

import androidx.room.TypeConverter
import com.anchietaalbano.thenewsapp.models.Source

class Converters {
    // Source não é um dado que pode ser colocado em um banco de dados, então temos que converte lo em um dado
    // que o banco de dados suporte

    @TypeConverter
    fun fromSource(source: Source): String{
        return source.name
    }

    @TypeConverter
    fun toSource(name : String): Source{
        return Source(name, name)
    }
}