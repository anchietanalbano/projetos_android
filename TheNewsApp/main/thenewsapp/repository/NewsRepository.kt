package com.anchietaalbano.thenewsapp.repository

import com.anchietaalbano.thenewsapp.api.RetrofitInstance
import com.anchietaalbano.thenewsapp.db.ArticleDatabase
import com.anchietaalbano.thenewsapp.models.Article

class NewsRepository(val db: ArticleDatabase) {

    //buscar noticia atráves do retrofit
    suspend fun getHeadlines(countryCode: String, pageNumber: Int) =
        RetrofitInstance.api.getHeadlines(countryCode, pageNumber)

    // pesquisa usando retrofit
    suspend fun searchNews(searchQuery: String, pageNumber: Int) =
        RetrofitInstance.api.searchForNews(searchQuery, pageNumber)

    suspend fun upsert(article: Article) = db.getArticleDao().upsert(article)

    fun getFavouritesNews() = db.getArticleDao().getAllArticles()

    suspend fun deleteArticle(article: Article) = db.getArticleDao().deleteArticle(article)

}