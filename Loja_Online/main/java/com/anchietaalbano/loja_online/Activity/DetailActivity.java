package com.anchietaalbano.loja_online.Activity;

import android.os.Bundle;
import android.os.Handler;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anchietaalbano.loja_online.Adapter.SizeAdapter;
import com.anchietaalbano.loja_online.Adapter.SliderAdapter;
import com.anchietaalbano.loja_online.Domain.ItemsDomain;
import com.anchietaalbano.loja_online.Domain.SliderItems;
import com.anchietaalbano.loja_online.Fragment.DescriptionFragment;
import com.anchietaalbano.loja_online.Fragment.ReviewFragment;
import com.anchietaalbano.loja_online.Fragment.SoldFragment;
import com.anchietaalbano.loja_online.Helper.ManagmentCart;
import com.anchietaalbano.loja_online.databinding.ActivityDetailBinding;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends BaseActivity {

    ActivityDetailBinding binding;
    private ItemsDomain object;
    private int numberOrdem;
    private ManagmentCart managmentCart;
    private Handler sliderHandle = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        managmentCart = new ManagmentCart(this);

        getBundles();
        banners();
        initSize();
        setupViewPager();
    }

    private void initSize() {
        ArrayList<String> list = new ArrayList<>();
        list.add("PP");
        list.add("P");
        list.add("M");
        list.add("G");
        list.add("GG");

        binding.recyclerSize.setAdapter(new SizeAdapter(list));
        binding.recyclerSize.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }

    private void banners() {
        ArrayList<SliderItems> sliderItems = new ArrayList<>();
        for (int i = 0; i < object.getPicUrl().size(); i++) {
            sliderItems.add(new SliderItems(object.getPicUrl().get(i)));
        }


        binding.viewpageSlider.setAdapter(new SliderAdapter(sliderItems, binding.viewpageSlider));
        binding.viewpageSlider.setClipToPadding(false);
        binding.viewpageSlider.setClipChildren(false);
        binding.viewpageSlider.setOffscreenPageLimit(3);
        binding.viewpageSlider.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

    }

    private void getBundles() {
        object = (ItemsDomain) getIntent().getSerializableExtra("object");
        binding.titleTxt.setText(object.getTitle());
        binding.priceTxt.setText("$" + object.getPrice());
        binding.ratingBar.setRating((float) object.getRating());
        binding.ratingTxt.setText(object.getRating() + " Avaliação");


        binding.addToCartBtn.setOnClickListener(view -> {
            object.setNumberinCart(numberOrdem);
            managmentCart.insertItems(object);
        });
        binding.backBtn.setOnClickListener(v -> finish());
    }

    private void setupViewPager(){
        ViewPagerAdapter adapter=new ViewPagerAdapter(getSupportFragmentManager());

        DescriptionFragment tab1 =new DescriptionFragment();
        ReviewFragment tab2 = new ReviewFragment();
        SoldFragment tab3 = new SoldFragment();

        Bundle bundle1 = new Bundle();
        Bundle bundle2 = new Bundle();
        Bundle bundle3 = new Bundle();

        bundle1.putString("description", object.getDescription());

        tab1.setArguments(bundle1);
        tab2.setArguments(bundle2);
        tab3.setArguments(bundle3);

        adapter.addFrag(tab1, "Descrição");
        adapter.addFrag(tab2, "Avaliações");
        adapter.addFrag(tab3, "Vendidos");

        binding.viewpager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewpager);
    }
    private class ViewPagerAdapter extends FragmentPagerAdapter{
        private final List<Fragment> myFragmentList = new ArrayList<>();
        private final List<String> myFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return myFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return myFragmentList.size();
        }

        private void addFrag(Fragment fragment, String title){
            myFragmentList.add(fragment);
            myFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position){
            return myFragmentTitleList.get(position);
        }
    }

}