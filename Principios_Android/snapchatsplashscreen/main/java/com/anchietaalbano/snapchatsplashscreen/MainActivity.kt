package com.anchietaalbano.snapchatsplashscreen

// intent explicita e intent implicita

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Thread.sleep(3000)
        installSplashScreen()
        setContentView(R.layout.activity_main)

        // intent explicita
        val explicitButton = findViewById<Button>(R.id.explicitButton)

        explicitButton.setOnClickListener{
            val explicitIntent = Intent(this, SecondActivity::class.java)
            startActivity(explicitIntent)
            finish()
        }
        // intent implicita
        val implicitIntent = findViewById<Button>(R.id.implicitButton)

        // a maneira de acessa o externo pode mudar cada desenvolvimento
        val url = "https://www.google.com"

        // passa uma ação direcionando para a url
        implicitIntent.setOnClickListener {
            val implicitIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(implicitIntent)

        }
    }
}