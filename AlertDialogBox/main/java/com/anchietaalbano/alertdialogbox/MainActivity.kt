package com.anchietaalbano.alertdialogbox

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val showButton: Button = findViewById(R.id.showButton)

        showButton.setOnClickListener{
            showAlertDialog()
        }

    }

    private fun showAlertDialog(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Snapchat")
            .setMessage("Você quer desinstalar o app?")
            .setPositiveButton("Sim"){ dialog, which ->
                Toast.makeText(this, "O app foi desinstalado com sucesso.", Toast.LENGTH_SHORT).show()
            }
            .setNegativeButton("Não"){ dialog, which ->
                dialog.dismiss()
            }
        val alertaDialog: AlertDialog = builder.create()
        alertaDialog.show()
    }
}