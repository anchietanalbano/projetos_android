package com.anchietaalbano.viewpagenext



import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView

import androidx.viewpager2.widget.ViewPager2

class MainActivity : AppCompatActivity() {

    // 1 = Declarar variaveis
    private lateinit var viewPager: ViewPager2
    private lateinit var pagerAdapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 6 criar o adapter
        viewPager = findViewById(R.id.viewPager)
        pagerAdapter = ViewPagerAdapter()
        viewPager.adapter = pagerAdapter

    }

}

// 2 criando viewpage adapter
class ViewPagerAdapter: RecyclerView.Adapter<Viewholder>(){

    // 4 - Criar lista de items
    private val itemList = listOf("Este é o primeiro item da lista -> > >",
        "< < <- Este é o segundo item da lista. -> > >",
        "< < <- Este é o terceiro item da lista.")

    // 5 - inplementar o members

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.page_layout, parent, false)
        return Viewholder(view);
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder : Viewholder, position: Int) {
        val item = itemList[position]
        holder.bind(item)
    }

}

// 3 - criando viewholder
class Viewholder (itemView: View): RecyclerView.ViewHolder(itemView){
    private val pagerText: TextView = itemView.findViewById(R.id.pageText)
    fun bind (item: String){
        pagerText.text= item
    }
}